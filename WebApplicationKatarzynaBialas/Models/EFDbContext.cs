﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplicationKatarzynaBialas.Models
{
    public class EFDbContext :DbContext
    {
        public DbSet<Pizza> Pizzas { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}