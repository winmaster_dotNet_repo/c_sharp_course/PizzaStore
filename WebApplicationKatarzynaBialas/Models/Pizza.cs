﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationKatarzynaBialas.Models
{
    public class Pizza
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Ingredients { get; set; }
        public int Price { get; set; }
        public bool VegeAlert { get; set; }
    }
}