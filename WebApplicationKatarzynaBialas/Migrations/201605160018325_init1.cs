namespace WebApplicationKatarzynaBialas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pizzas", "Price", c => c.Int(nullable: false));
            AddColumn("dbo.Pizzas", "VegeAlert", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Pizzas", "VegeAlert");
            DropColumn("dbo.Pizzas", "Price");
        }
    }
}
