namespace WebApplicationKatarzynaBialas.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                 "dbo.Pizzas",
                 c => new
                 {
                     Id = c.Int(nullable: false, identity: true),
                     Name = c.String(),
                     Ingredients = c.String(),
                     VegeAlert = c.Boolean(),
                     Price = c.Int(nullable: true)
                    })
                .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
            DropTable("dbo.Pizzas");
        }
    }
}

//metody upadate-migrations